export class Paginator {
    current_page: Number;
    data:any[];
    first_page_url: string;
    from: string;
    last_page: Number;
    last_page_url: string;
    next_page_url: string;
    path: string;
    per_page: Number;
    prev_page_url: string;
    to: Number;
    total: Number;
}
