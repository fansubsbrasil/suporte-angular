export class Chamado {
    id:string;
    titulo: string;
    conteudo: string;
    autor: string;
    numero_chamado:string;
    departamento: string;
    prioridade: string;
    proprietario:string;
    created_at: string;
    updated_at: string;
}
