export class Artigo {
   
    id:Number;
    titulo:string;
    descricao: string;
    classe: string;
    created_at: string;
    updated_at: string;
}
