import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError, flatMap} from "rxjs/operators";
import {AuthService} from "./auth.service";
import {TokenInterceptorService} from "./token-interceptor.service";

@Injectable({
    providedIn: 'root'
})
export class RefreshTokenInterceptorService implements HttpInterceptor {

    constructor(private auth: AuthService, private tokenInterceptor: TokenInterceptorService) {
    }

    //interceptor refresh token


    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log("t3");
        return next.handle(req)
            .pipe(
                catchError(error => {
                    const responseError = error as HttpErrorResponse; 
                    if (responseError.status === 401) {
                        return this.refreshToken(req, next);
                    }
                    //this.auth.removerToken();
                    //return throwError({ error: { message: 'Username or password is incorrect' } });
                    return throwError(error)
                })
            )
     
    }

    refreshToken(req: HttpRequest<any>, next: HttpHandler) {
        console.log("t4");
        return this.auth
            .refresh()
            .pipe(
                flatMap(() => this.tokenInterceptor.intercept(req, next))
            )
    }


}