import { LocalStorageService } from './local-storage.service';
import { Router } from '@angular/router';
import { Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { delay, tap, take, map} from 'rxjs/operators';


@Injectable()
export class AuthService {

  
 
  constructor(private http: HttpClient, private storage: LocalStorageService) {
  }

  login(user) {
      return this.http.post<{ access_token: string,refresh_token:string }>('http://127.0.0.1:8000/oauth/token', user)
          .pipe(
              delay(2000),
              tap(data=> this.armazenarToken(data)),take(1));
  }

  refresh() {
      return this.http.post<{ token: string }>('http://localhost:8000/api/refresh', {})
          .pipe(
              tap(data => this.storage.set('token', data.token)),
              take(1)
          );
  }

armazenarToken(data:any){
    console.log(data)
    this.storage.set('token', data.access_token)
    this.storage.set('refresh', data.refresh_token)
}
hasToken() {
    return window.localStorage.getItem('token') !== null;
}
removerToken(){
    this.storage.remove('token');
    this.storage.remove('refresh');
}
}