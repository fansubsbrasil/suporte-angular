import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

    constructor() {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        //Se o hasToken for nulo entra no getAuthRequest
        const newRequest = this.hasToken() ? this.getAuthRequest(req) : req;
        //console.log("t1"+newRequest.body);
        return next.handle(newRequest);
    }

    hasToken() {
        return window.localStorage.getItem('token') !== null;
    }

    getAuthRequest(req: HttpRequest<any>) {
        console.log("t2");
        return req.clone({
            headers: req.headers.set('Authorization','Bearer ' + window.localStorage.getItem('token'))         
        })
    }
}