import { AuthService } from './../services/auth.service';
import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, Route} from "@angular/router";
import {Observable} from "rxjs";

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(private router: Router,private auth:AuthService) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (!this.auth.hasToken()) {
            console.log(this.auth.hasToken())
            this.router.navigate(['/login']);
            return false;
        }
        return true;

        //esta expirado
        // return this.auth.refresh().pipe(
        //     map(data) => true,
        //     catchError(() => {
        //         this.router.navigate(['/login'])
        //         throwError(false)
        //     })
        // )
    }

   
    //canLoad(route: Route): Observable<boolean>|Promise<boolean>|boolean {
      //console.log('canLoad: verificando se usuário pode carregar o cod módulo');
     // if (!this.hasToken()) {
      //  this.router.navigate(['/login']);
      //  return false;
   // }
   // }
}
