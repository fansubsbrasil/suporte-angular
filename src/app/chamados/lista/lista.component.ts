import { ChamadoService } from './../chamado.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Chamado } from 'src/app/models/chamado';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {
  chamados$:Observable<Chamado[]>;
  constructor(
private servico:ChamadoService

  ) { }

  ngOnInit() {
    this.chamados$ = this.servico.list().pipe();
  }

}
