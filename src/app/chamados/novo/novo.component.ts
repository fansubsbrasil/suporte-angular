import { Chamado } from './../../models/chamado';
import { CrudService } from './../../services/crud.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChamadoService } from '../chamado.service';
import { ChamadosModule } from '../chamados.module';

@Component({
  selector: 'app-novo',
  templateUrl: './novo.component.html',
  styleUrls: ['./novo.component.css']
})
export class NovoComponent implements OnInit {
  chamados:Chamado;
  form: FormGroup;
  public editorConfig = {
    theme: 'bubble',
    placeholder: "输入任何内容，支持html",
    modules: {
      toolbar: [
        ['bold', 'italic', 'underline', 'strike'],
        [{ 'list': 'ordered'}, { 'list': 'bullet' }],
        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
        [{ 'color': [] }, { 'background': [] }],
        [{ 'font': [] }],
        [{ 'align': [] }],
        ['link', 'image'],
        ['clean']
      ]
    }
  };
  constructor(
    private servico:ChamadoService,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.form = this.fb.group({  
      id:[''],
      titulo: ['this.artigos.titulo',[Validators.minLength(3), Validators.maxLength(250)]],
      conteudo: ['this.artigos.descricao',[Validators.minLength(3), Validators.maxLength(500)]],
      autor:  ['this.artigos.classe',[ Validators.minLength(3), Validators.maxLength(250)]], 
      numero_chamado:[''],
      departamento:[''],
      prioridade: [''],
      proprietario:[''],
      created_at: [''],
      updated_at:[''],
    });
  }
salvar(){
  this.chamados = this.form.value;
  console.log(this.chamados);
  this.servico.save(this.chamados).subscribe();
}
}
