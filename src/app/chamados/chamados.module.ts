import { ChamadoService } from './chamado.service';
import { Artigo } from './../models/artigo';
import { CrudService } from './../services/crud.service';
import { DetalheComponent } from './detalhe/detalhe.component';
import { NovoComponent } from './novo/novo.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChamadosRoutingModule } from './chamados-routing.module';
import { ListaComponent } from './lista/lista.component';
import { MzCardModule, MzButtonModule, MzInputModule, MzTextareaModule, MzCollectionModule, MzToastModule } from 'ngx-materialize';
import { NgxPaginationModule } from 'ngx-pagination';
import { ReactiveFormsModule } from '@angular/forms';




@NgModule({
  declarations: [
    NovoComponent,
    DetalheComponent,
    ListaComponent
  ],
  imports: [
    CommonModule,
    ChamadosRoutingModule,
    MzCardModule,
    MzButtonModule,   
    MzInputModule,
    MzTextareaModule,
    MzCollectionModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    MzToastModule,
   
  ],
  providers: [
ChamadoService
  ]
})
export class ChamadosModule { }
