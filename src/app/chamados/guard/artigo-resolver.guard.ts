import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable} from 'rxjs';
import { Chamado } from 'src/app/models/chamado';
import { ChamadoService } from '../chamado.service';

@Injectable({
  providedIn: 'root'
})
export class ChamadoResolverGuard implements Resolve<Chamado> {
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Chamado | Observable<Chamado> | Promise<Chamado> {
    throw new Error("Method not implemented.");
  }
  constructor(private service: ChamadoService) {}

  //resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Artigo> {
    //if (route.params && route.params['id']) {
//return "test"
      //return this.service.loadByID(route.params['id']);
   // }

    //return of({
      //  id: null,
       // titulo: null,
       // descricao: null,
       // classe: null,
       // created_at: null,
       // updated_at: null,
    //});
  //}
}