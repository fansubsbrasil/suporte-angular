import { Chamado } from './../models/chamado';
import { HttpClient } from '@angular/common/http';
import { CrudService } from '../services/crud.service';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChamadoService extends CrudService<Chamado>{

  constructor(protected http:HttpClient) {
    super(http,`${environment.API}chamado`);
  }
}
