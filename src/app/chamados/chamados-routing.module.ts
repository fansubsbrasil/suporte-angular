import { ListaComponent } from './lista/lista.component';
import { DetalheComponent } from './detalhe/detalhe.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NovoComponent } from './novo/novo.component';
import { AuthGuardService } from '../auth/auth.guard';

const routes: Routes = [
  { path: '', component: ListaComponent },
  { path: 'detalhe', component: DetalheComponent},
  {path:'novo', component: NovoComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService]
})
export class ChamadosRoutingModule { }
