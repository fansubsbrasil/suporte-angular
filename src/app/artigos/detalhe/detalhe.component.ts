import { Artigo } from './../../models/artigo';
import { ArtigoService } from './../artigo.service';
import {ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { switchMap, map } from 'rxjs/operators';
import { MzToastService } from 'ngx-materialize';

@Component({
  selector: 'app-detalhe',
  templateUrl: './detalhe.component.html',
  styleUrls: ['./detalhe.component.css']
})
export class DetalheComponent implements OnInit {
  
  constructor  (
    private route: ActivatedRoute,
    private servico:ArtigoService,
    private router: Router,
    private toastService: MzToastService,
    ) { 

    }
   artigo = this.route.snapshot.data['artigo'];
  ngOnInit() {  
   
    console.log(this.artigo)
  }
excluir(){
  this.servico.remove(this.artigo.id).subscribe(
  data=>(this.showToast())
  );
}
editar(){
  
  this.router.navigate(['/artigos/novo',this.artigo])
}
showToast() {
  this.toastService.show('Excluido com sucesso!', 4000, 'red');
  this.router.navigate(['/artigos'])
}
}
