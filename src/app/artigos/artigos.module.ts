import { ArtigoService } from './artigo.service';
import { NovoComponent } from './novo/novo.component';
import { ListaComponent } from './lista/lista.component';
import { DetalheComponent } from './detalhe/detalhe.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgxPaginationModule} from 'ngx-pagination';
import { ArtigosRoutingModule } from './artigos-routing.module';
import { MzCardModule, MzButtonModule, MzInputModule, MzTextareaModule, MzCollectionModule, MzModalModule, MzPaginationModule, MzToastModule } from 'ngx-materialize';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    DetalheComponent,
    ListaComponent,
    NovoComponent
  ],
  imports: [
    CommonModule,
    ArtigosRoutingModule,
    MzCardModule,
    MzButtonModule,   
    MzInputModule,
    MzTextareaModule,
    MzCollectionModule,
    MzModalModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    MzToastModule 
  ],
  providers:[
    ArtigoService
  ]
})
export class ArtigosModule { }
