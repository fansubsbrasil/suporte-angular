import { Artigo } from './../../models/artigo';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { ArtigoService } from '../artigo.service';

@Injectable({
  providedIn: 'root'
})
export class ArtigoResolverGuard implements Resolve<Artigo> {
  constructor(private service: ArtigoService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Artigo> {
    if (route.params && route.params['id']) {

      return this.service.loadByID(route.params['id']);
    }

    return of({
        id: null,
        titulo: null,
        descricao: null,
        classe: null,
        created_at: null,
        updated_at: null,
    });
  }
}