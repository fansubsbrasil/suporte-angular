import { HttpClient } from '@angular/common/http';
import { Artigo } from './../models/artigo';
import { CrudService } from './../services/crud.service';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ArtigoService extends CrudService<Artigo>{

  constructor(protected http:HttpClient) {
    super(http,`${environment.API}artigo`);
  }
}
