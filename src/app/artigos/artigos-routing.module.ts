import { ListaComponent } from './lista/lista.component';
import { NovoComponent } from './novo/novo.component';
import { DetalheComponent } from './detalhe/detalhe.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArtigoResolverGuard } from './guard/artigo-resolver.guard';

const routes: Routes = [
  { path: '', component: ListaComponent },
  {path:'novo', component: NovoComponent},
  {path:'novo/:id', component: NovoComponent},
  { path:':id', component: DetalheComponent,
  resolve: {
    artigo: ArtigoResolverGuard
  }
},
 
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArtigosRoutingModule { }
