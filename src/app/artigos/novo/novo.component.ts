import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArtigoService } from '../artigo.service';
import { Artigo } from 'src/app/models/artigo';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MzToastService } from 'ngx-materialize';

@Component({
  selector: 'app-novo',
  templateUrl: './novo.component.html',
  styleUrls: ['./novo.component.css']
})
export class NovoComponent implements OnInit {
  artigos: Artigo;
  form: FormGroup;

  constructor(
    private router: Router,
    private servico:ArtigoService,
    private fb: FormBuilder,
    private toastService: MzToastService,
    private route: ActivatedRoute,
    ) { 
     
      this.artigos = this.route.params['value'];
  console.log(this.artigos.id)
   this.form = this.fb.group({  
      id:[this.artigos.id],
      titulo: [this.artigos.titulo,[Validators.minLength(3), Validators.maxLength(250)]],
      descricao: [this.artigos.descricao,[Validators.minLength(3), Validators.maxLength(500)]],
      classe:  [this.artigos.classe,[ Validators.minLength(3), Validators.maxLength(250)]],
    });


  }

  ngOnInit() {

  }

  salvar(){
    //if( this.artigos.id == undefined){

    this.artigos = this.form.value;
    //this.artigos.id = this.route.params['value']['id'];
    console.log(this.artigos)
    this.servico.save(this.artigos).subscribe(
      () => {
       this.showToast();
      }      
      );
    //}
  }

  showToast() {
    if(!this.artigos.id){
      this.toastService.show('Cadastrado com sucesso!', 4000, 'green');
    }else{
      this.toastService.show('Atualizado com sucesso!', 4000, 'blue');
    }
   
    this.router.navigate(['/artigos'])
  }
  }




