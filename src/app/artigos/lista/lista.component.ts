import { Paginator } from './../../models/paginator';
import { ArtigoService } from './../artigo.service';
import { Artigo } from './../../models/artigo';
import { CrudService } from './../../services/crud.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { tap, map } from 'rxjs/operators';


@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})

export class ListaComponent implements OnInit{

  artigos$:Observable<Artigo[]>;

  constructor(
    private servico:ArtigoService,   
    private router: Router,
    private route: ActivatedRoute,
   
    ) { 
      
    }
 
  ngOnInit() {
//this.paginator = this.route.snapshot.data['artigo'];
this.artigos$ = this.servico.list().pipe();
  }
clicar(){
  this.router.navigate(['/artigos/novo'], { relativeTo: this.route });
}



}
