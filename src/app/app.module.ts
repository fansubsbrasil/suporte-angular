
import { AlertService } from './services/alert.service';
import { CrudService } from './services/crud.service';
import { LocalStorageService } from './services/local-storage.service';
import { LoginComponent } from './login/login.component';
import { AuthService } from './services/auth.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuardService } from './auth/auth.guard';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MzInputModule, MzSidenavModule, MzNavbarModule, MzCollectionModule } from 'ngx-materialize';
import { MzCardModule } from 'ngx-materialize';
import { NavbarComponent } from './navbar/navbar.component';
import { TokenInterceptorService } from './services/token-interceptor.service';
import { RefreshTokenInterceptorService } from './services/refresh-token-interceptor.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AlertComponent } from './shared/alert/alert.component';
import { MzModalModule } from 'ngx-materialize'

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    DashboardComponent,
    AlertComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MzInputModule,
    MzCardModule,
    MzSidenavModule,
    MzNavbarModule,
    MzModalModule,

    
  ],
  providers: [AuthGuardService,
  AuthService,
LocalStorageService,
AlertService,

{
  provide: HTTP_INTERCEPTORS,
  useClass: TokenInterceptorService,
  multi: true
},
{
  provide: HTTP_INTERCEPTORS,
  useClass: RefreshTokenInterceptorService,
  multi: true
},
],
  bootstrap: [AppComponent]
})
export class AppModule { }
