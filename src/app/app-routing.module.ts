import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './auth/auth.guard';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'chamados', 
  loadChildren: () => import('./chamados/chamados.module').then(mod => mod.ChamadosModule),
  canActivate: [AuthGuardService]
  },
  { path: 'artigos', 
  loadChildren: () => import('./artigos/artigos.module').then(mod => mod.ArtigosModule),
  canActivate: [AuthGuardService]
  },
  { path: '', redirectTo: '/chamados', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
