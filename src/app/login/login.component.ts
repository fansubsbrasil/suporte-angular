import { User } from './../models/user';
import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { FormsModule } from '@angular/forms';

import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AlertService } from '../services/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  isLoginError : boolean = false;
User = {
    username: '',
    password: '',
    grant_type:'password',
    client_secret:'JwfJfGYLzAYoqTdtFZUXVEcZSV8O0NM0vvRcpolj',
    client_id:'2',
  };
  constructor(private auth: AuthService, private router: Router,
    private alertService: AlertService) { }


  ngOnInit() {
    if (this.auth.hasToken()) {
      console.log(this.auth.hasToken())
      this.router.navigate(['/chamados']);
  }

  }
  //teste(){
   // this.http.post("http://localhost:8000/oauth/token",body,httpOptions).toPromise().then(
   // response => {
    //  console.log(response)
   // }
   // ).catch(Response => console.log("Response.valueOf()"));
  
    // }

     login() {
      this.isLoginError = false;
      this.auth.login(this.User)
          .subscribe(() => {
              this.router.navigate(['/chamados'])
             
          },
          error => {
            console.log(error);
            //this.alertService.error(error);
            this.isLoginError = true;
        });
  }

}
